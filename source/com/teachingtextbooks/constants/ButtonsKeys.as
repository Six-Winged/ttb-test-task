package com.teachingtextbooks.constants {

public class ButtonsKeys {

    public static const DEFAULT: String = 'default';
    public static const ARROW_UP: String = 'arrow-up';
    public static const ARROW_DOWN: String = 'arrow-down';
    public static const FLAT: String = 'flat';

}
}
