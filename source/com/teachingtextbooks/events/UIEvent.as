package com.teachingtextbooks.events {

import flash.events.Event;

public class UIEvent extends Event {

    public var data: Object;

    public static const SCROLLED: String = 'scrolled';

    public function UIEvent (type: String, bubbles: Boolean=false, cancelable: Boolean=false, data: Object=null) {
        super (type, bubbles, cancelable);
        this.data = data;
    }
}

}
