package com.teachingtextbooks.main {

import com.teachingtextbooks.constants.ButtonsKeys;
import com.teachingtextbooks.ui.ListScrollBar;
import com.teachingtextbooks.ui.SelectMenuUI;
import com.teachingtextbooks.utils.ButtonsFactory;
import com.teachingtextbooks.utils.ScrollBarConfig;
import com.teachingtextbooks.utils.SelectMenuUIConfig;
import com.teachingtextbooks.utils.UIFactory;

import flash.display.SimpleButton;

import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageQuality;
import flash.display.StageScaleMode;
import flash.events.Event;

import net.hires.debug.Stats;

[SWF(width="740",height="580",frameRate="60",backgroundColor="#00CCFF")]
public class TestTask extends Sprite {

    public function TestTask() {
        stage.quality = StageQuality.BEST;
        stage.showDefaultContextMenu = false;
        stage.align = StageAlign.TOP_LEFT;
        stage.scaleMode = StageScaleMode.NO_SCALE;
        loaderInfo.addEventListener(Event.COMPLETE, loadingHandler);

        const stats: Stats = new Stats;
        stats.addEventListener(Event.ADDED_TO_STAGE, function(ev: Event): void {
            stats.x = stage.stageWidth - stats.width;
            stats.y = stage.stageHeight - stats.height;
        });
        addChild(stats);

    }

    protected function loadingHandler(ev: Event): void {
/*
        const uiFactory: UIFactory = UIFactory.getInstance();
        const tblock: Sprite = new selectmenu_label_test;
        const tmask: Sprite = new test_mask;
        tblock.x = 100;
        tblock.y = 40;
        tmask.width = tblock.width;
        tmask.height = 200;
        tmask.x = tblock.x;
        tmask.y = tblock.y;
        tblock.mask = tmask;

        const scroll: ListScrollBar = uiFactory.getScrollBar(
            new ScrollBarConfig(20, 200, 3, 5, tblock, 100, tmask)
        );

        addChild(tblock);
        addChild(tmask);
        addChild(scroll);
        scroll.mouseEnabled = true;
*/

        const menuConfig: SelectMenuUIConfig = new SelectMenuUIConfig(12, 0xfcc000, 10, 10, 10, 15, 15);

        var menu: SelectMenuUI = new SelectMenuUI(menuConfig);
        menu.name = 'menu-0';
        menu.move(20, 20);
        menu.scale(.6);
        menu.populate([
            {txt:'January', val:'1'},
            {txt:'February', val:'2'},
            {txt:'March', val:'3'},
            {txt:'April', val:'4'},
            {txt:'May', val:'5'},
            {txt:'June', val:'6'},
            {txt:'July', val:'7'},
            {txt:'August', val:'8'},
            {txt:'September', val:'9'},
            {txt:'October', val:'10'},
            {txt:'November', val:'12'},
            {txt:'December', val:'12'}
        ]);
        addChild(menu);

        menu = new SelectMenuUI(menuConfig);
        menu.name = 'menu-1';
        menu.move(220, 20);
        menu.scale(.6);
        menu.populate([
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ]);
        addChild(menu);

        menu = new SelectMenuUI(menuConfig);
        menu.name = 'menu-1';
        menu.move(420, 20);
        menu.scale(.6);
        menu.populate([
            'Alabama',
            'Alaska',
            'Arizona',
            'Arkansas',
            'California',
            'Colorado',
            'Connecticut',
            'Delaware',
            'Florida',
            'Georgia',
            'Hawaii',
            'Idaho',
            'Illinois',
            'Indiana',
            'Iowa',
            'Kansas',
            'Kentucky',
            'Louisiana',
            'Maine',
            'Maryland',
            'Massachusetts',
            'Michigan',
            'Minnesota',
            'Mississippi',
            'Missouri',
            'Montana',
            'Nebraska',
            'Nevada',
            'New Hampshire',
            'New Jersey',
            'New Mexico',
            'New York',
            'North Carolina',
            'North Dakota',
            'Ohio',
            'Oklahoma',
            'Oregon',
            'Pennsylvania',
            'Rhode Island',
            'South Carolina',
            'South Dakota',
            'Tennessee',
            'Texas',
            'Utah',
            'Vermont',
            'Virginia',
            'Washington',
            'West Virginia',
            'Wisconsin',
            'Wyoming'
        ]);
        addChild(menu);
    }
}

}
