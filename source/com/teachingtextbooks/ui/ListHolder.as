package com.teachingtextbooks.ui {

import caurina.transitions.Tweener;

import flash.display.BlendMode;
import flash.display.Sprite;

public class ListHolder extends Sprite {

    protected var back: Sprite;
    protected var edge: Sprite;
    protected var holder: Sprite;
    protected var bshadow: Sprite;
    protected var rshadow: Sprite;

    protected var item: ListItem;

    public function ListHolder(marker: Sprite, shadowed: Boolean) {
        back = new selectmenu_back;
        edge = new selectmenu_ragged_edge;
        holder = new Sprite;

        if (shadowed) {
            bshadow = new selectmenu_listshadow_bottom;
            rshadow = new selectmenu_listshadow_right();
            bshadow.visible = rshadow.visible = false;

            bshadow.blendMode = rshadow.blendMode = BlendMode.MULTIPLY;
            addChild(rshadow);
            addChild(bshadow);
        }

        addChild(back);
        addChild(edge);
        marker && holder.addChild(marker);
        addChild(holder);
    }

    public function open(time: Number, delay: Number = 0): void {
        Tweener.removeTweens(this);
        Tweener.addTween(this, {y: 0, time: time, delay: delay, transition: 'easeOutCubic'});
    }
    public function close(time: Number, delay: Number = 0): void {
        Tweener.removeTweens(this);
        Tweener.addTween(this, {y: -height, time: time, delay: delay, transition: 'easeOutCubic'});
    }
    public function reset(): void {
        Tweener.removeTweens(this);
        y = -height;
    }
    public function dispose(): void {
        reset();
        parent && parent.removeChild(this);
    }

    public function addItem(obj: ListItem): void {
        item && removeChild(item);
        addChild(obj);
        item = obj;
    }
    public function setLabel(str: String): void {
        item && item.setLabel(str);
    }

    public function setSize(W: int, H: int): void {
        back.width = W;
        back.height = H;
        edge.width = back.width;
        edge.y = int(back.height - edge.height/2);

        if (bshadow) {
            bshadow.width = back.width + 24;
            bshadow.x = (back.width - bshadow.width) / 2;
            bshadow.y = back.height - 2;
            bshadow.visible = true;
        }
        if (rshadow) {
            rshadow.height = back.height;
            rshadow.x = back.width;
            rshadow.y = back.y;
            rshadow.visible = true;
        }
    }

    public function getBack(): Sprite {
        return back;
    }
    public function getHolder(): Sprite {
        return holder;
    }

}
}
