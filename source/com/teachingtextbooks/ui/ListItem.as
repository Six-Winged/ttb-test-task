package com.teachingtextbooks.ui {

import com.teachingtextbooks.utils.SelectMenuUIConfig;

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

public class ListItem extends Sprite {

    protected var back: Sprite;
    protected var label: Sprite;
    protected var field: TextField;

    public function ListItem(data: Object, arrowed: Boolean = false, config: SelectMenuUIConfig = null, tapHandler: Function = null) {
        back = new Sprite;
        back.graphics.beginFill(0xeeeeee, 0);
        back.graphics.drawRect(0, 0, 1, 1);
        back.graphics.endFill();

        label = new selectmenu_label;
        field = label.getChildByName('txt') as TextField;
        field.autoSize = TextFieldAutoSize.CENTER;
        field.text = data.txt;
//        field.border = true;
        field.x = 0;

        const arrow: Sprite = new selectmenu_arrow;
        arrow.x = config ? field.width + config.arrowGap : field.width;
        arrow.y = int((field.height - arrow.height) / 2 + 8);
        arrow.visible = arrowed;

        mouseChildren = false;

        addChild(back);
        addChild(label);
        label.addChild(arrow);

        tapHandler && addEventListener(MouseEvent.CLICK, tapHandler);
    }
    public function setLabel(str: String): void {
        field.text = str;
    }
    public function setSize(W: int, H: int): void {
        back.width = W;
        back.height = H;
        label.x = int((W - label.width) / 2);
    }

}
}
