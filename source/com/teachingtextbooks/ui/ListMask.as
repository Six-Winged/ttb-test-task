package com.teachingtextbooks.ui {

import flash.display.Sprite;

public class ListMask extends Sprite {

    public function ListMask() {}

    public function init(W: Number, H: Number): void {
        reset();
        graphics.beginFill(0x0e0e0e, .5);
        graphics.drawRect(0, 0, W, H);
        graphics.endFill();
        mouseEnabled = false;
    }
    public function reset(): void {
        graphics.clear();
    }

}
}
