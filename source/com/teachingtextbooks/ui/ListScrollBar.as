package com.teachingtextbooks.ui {

import caurina.transitions.Tweener;

import com.teachingtextbooks.constants.ButtonsKeys;
import com.teachingtextbooks.events.UIEvent;
import com.teachingtextbooks.utils.ButtonsFactory;
import com.teachingtextbooks.utils.ScrollBarConfig;
import com.teachingtextbooks.utils.UIFactory;

import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.utils.clearInterval;
import flash.utils.setInterval;

public class ListScrollBar extends Sprite {

    protected var config: ScrollBarConfig;

    protected var targetPosition: Point;
    protected var timer: int;

    protected var up: SimpleButton;
    protected var down: SimpleButton;
    protected var track: SimpleButton;
    protected var thumb: ListScrollBarThumb;
    protected var trackBack: Sprite;

    public function ListScrollBar(_config: ScrollBarConfig) {
        config = _config.clone();
        targetPosition = new Point(config.target.x, config.target.y);

        const uiFactory: UIFactory = UIFactory.getInstance();
        const buttonsFactory: ButtonsFactory = ButtonsFactory.getInstance();

        up = buttonsFactory.getButton(ButtonsKeys.ARROW_UP);
        up.name = 'up';
        up.x = int((config.width-up.width)/2);
        up.addEventListener(MouseEvent.MOUSE_DOWN, buttonHandler);

        down = buttonsFactory.getButton(ButtonsKeys.ARROW_DOWN);
        down.name = 'down';
        down.x = int((config.width - down.width) / 2);
        down.y = int(config.height - down.height);
        down.addEventListener(MouseEvent.MOUSE_DOWN, buttonHandler);

        const middleHeight: int = config.height - up.height - down.height - config.gap * 2;

        track = buttonsFactory.getButton(
            ButtonsKeys.FLAT,
            config.width,
            middleHeight
        );
        track.name = 'track';
        track.y = up.height + config.gap;
        track.addEventListener(MouseEvent.MOUSE_DOWN, buttonHandler);

        trackBack = uiFactory.getScrollTrack();
        trackBack.height = middleHeight;
        trackBack.x = int((config.width - trackBack.width) / 2);
        trackBack.y = up.height + config.gap;

        thumb = uiFactory.getScrollBarThumb();
        thumb.setPosition(
            int((config.width - thumb.width) / 2),
            trackBack.y
        );
        if (!config.fixedThumb) {
            thumb.height = trackBack.height * (config.mask.height / (config.target.height - _config.targetGap));
        }
        thumb.setMaxPosition(
            int((config.width - thumb.width) / 2),
            trackBack.y + middleHeight - thumb.height
        );
        thumb.addEventListener(UIEvent.SCROLLED, scrollHandler);

        timer = -1;

        addChild(trackBack);
        addChild(track);
        addChild(up);
        addChild(down);
        addChild(thumb);

        if (config.autoPosition) {
            x = config.target.x + config.target.width + config.padding;
            y = config.target.y;
        }

        mouseEnabled = false;
    }

    public function scrollTo(percent: Number): void {
        if (percent < 0) percent = 0;
        else if (percent > 1) percent = 1;
        thumb.setPercent(percent);
    }
    public function dispose(): void {
        clearInterval(timer);
        timer = -1;
        mouseEnabled = false;
        config.target.y = targetPosition.y;
        config.dispose();
    }

    protected function buttonHandler(arg: Object): void {
        const target: String = typeof arg === 'string' ? String(arg) : arg.target.name;
        switch(target) {
            case 'up':
                thumb.setPercent(thumb.getPercent()-.1);
                break;
            case 'down':
                thumb.setPercent(thumb.getPercent()+.1);
                break;
            case 'track':
                globalToLocal(new Point(stage.mouseX, stage.mouseY)).y > (thumb.y + thumb.height/2) ?
                    thumb.setPercent(thumb.getPercent()+.1) :
                    thumb.setPercent(thumb.getPercent()-.1);
                break;
        }
        if (timer === -1) {
            stage.addEventListener(MouseEvent.MOUSE_UP, stageHandler);
            timer = setInterval(buttonHandler, config.holdInterval, target);
        }
    }
    protected function stageHandler(ev: Event): void {
        clearInterval(timer);
        timer = -1;
    }
    protected function scrollHandler(ev: UIEvent): void {
        const newY: Number = targetPosition.y - (config.target.height + config.targetGap - config.mask.height) * thumb.getPercent();

        if (config.tweenScroll) {
            Tweener.removeTweens(config.target);
            Tweener.addTween(config.target, {y: newY, time: config.tweenTime, transition: 'easeOutCubic'})
        } else {
            config.target.y = newY;
        }
    }
    protected function wheelHandler(ev: MouseEvent): void {
        thumb.setPercent(thumb.getPercent()-ev.delta/10);
    }

    public function scale(val: Number): void {
        scaleX = scaleY = val;
    }
    public function enabled(bool: Boolean): void {
        mouseEnabled = bool;
        mouseChildren = bool;
        thumb.mouseEnabled = bool;
        bool ?
            config.target.addEventListener(MouseEvent.MOUSE_WHEEL, wheelHandler) :
            config.target.removeEventListener(MouseEvent.MOUSE_WHEEL, wheelHandler);
    }

}
}
