package com.teachingtextbooks.ui {

import com.teachingtextbooks.events.UIEvent;

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;

public class ListScrollBarThumb extends Sprite {

    protected var holder: Sprite;
    protected var movie: Sprite;
    protected var icon: Sprite;
    protected var targetPosition: Point;
    protected var position: Point;
    protected var maxPosition: Point;
    protected var pressed: Point;

    protected static const CORNER: uint = 6;

    public function ListScrollBarThumb() {
        holder = new Sprite;

        movie = new scroll_thumb;
        movie.scale9Grid = new Rectangle(CORNER, CORNER, movie.width - CORNER * 2, movie.height - CORNER * 2);

        icon = new scroll_thumb_icon;
        icon.x = int((movie.width - icon.width) / 2) + 1;

        addChild(holder);
        holder.addChild(movie);
        holder.addChild(icon);
        height = movie.height;

        position = new Point(0, 0);
        maxPosition = new Point(0, 0);
        pressed = new Point(0, 0);

        useHandCursor = true;
    }

    override public function get y(): Number {
        return holder.y;
    }
    override public function set mouseEnabled(bool: Boolean): void {
        super.mouseEnabled = bool;
        bool && !hasEventListener(MouseEvent.MOUSE_DOWN) ?
            addEventListener(MouseEvent.MOUSE_DOWN, downHandler) :
            removeEventListener(MouseEvent.MOUSE_DOWN, downHandler);
    }

    public function setPosition(X: int, Y: int): void {
        holder.x = X;
        holder.y = Y;
        position.setTo(X, Y);
    }
    public function setMaxPosition(X: int, Y: int): void {
        maxPosition.setTo(X, Y);
    }
    public function getPercent(): Number {
        return (holder.y - position.y) / (maxPosition.y - position.y);
    }
    public function setPercent(val: Number): void {
        if (val < 0) val = 0;
        else if (val > 1) val = 1;
        updatePosition(position.y + (maxPosition.y - position.y) * val);
    }

    protected function downHandler(ev: MouseEvent): void {
        pressed.setTo(holder.mouseX, holder.mouseY);
        stage.addEventListener(MouseEvent.MOUSE_MOVE, moveHandler);
        stage.addEventListener(MouseEvent.MOUSE_UP, upHandler);
    }
    protected function upHandler(ev: MouseEvent): void {
        stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveHandler);
        stage.removeEventListener(MouseEvent.MOUSE_UP, upHandler);
    }
    protected function moveHandler(ev: MouseEvent): void {
        const stagePos: Point = globalToLocal(new Point(stage.mouseX, stage.mouseY));
        updatePosition(stagePos.y - pressed.y);
        ev.updateAfterEvent();
    }
    protected function updatePosition(newY: Number): void {
        if (newY < position.y) newY = position.y;
        else if (newY > maxPosition.y) newY = maxPosition.y;
        holder.y = newY;
        dispatchEvent(new UIEvent(UIEvent.SCROLLED));
    }

    override public function set height(val: Number): void {
        if (val < CORNER * 3) val = CORNER * 3;
        movie.height = val;
        icon.y = int((movie.height - icon.height) / 2);
    }


}
}
