/**
This class is for a custom select menu UI.
I am just going to write a few needed properties and method names here.
It is up to the coder to write the functions!
Direct any questions to seang@techingtextbooks.com
**/
package com.teachingtextbooks.ui {

import com.teachingtextbooks.utils.ScrollBarConfig;
import com.teachingtextbooks.utils.SelectMenuUIConfig;
import com.teachingtextbooks.utils.UIFactory;

import flash.display.BlendMode;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.utils.Dictionary;

public class SelectMenuUI extends Sprite {

    public var _options:Array; // an array of all the options
    public var _selected_index:int; // the index of the currently selected option
    public var _selected_text:String; // the text of the currently selected option
    public var _selected_value:String; // the value of the currently selected option

    protected var _top: Sprite;
    protected var _selection: ListHolder;
    protected var _marker: Sprite;
    protected var _menu: ListHolder;
    protected var _holder: Sprite;
    protected var _scroll: ListScrollBar;

    protected var _items: Vector.<Sprite>;
    protected var _itemsHash: Dictionary;
    protected var _values: Vector.<Object>;

    protected var _config: SelectMenuUIConfig;
    protected var _mask: ListMask;
    protected var _inMask: ListMask;
    protected var _masked: Sprite;
    protected var _state: String;

    protected static const OPENED: String = 'opened';
    protected static const CLOSED: String = 'closed';
    protected static const APPEAR_TIME: Number = .4;
    protected static const DISAPPEAR_TIME: Number = .3;

    // CONSTRUCTOR
    public function SelectMenuUI(config: SelectMenuUIConfig) {
        _config = config.clone();
        _items = new <Sprite>[];
        _values = new <Object>[];
        reset();
        addEventListener(Event.ADDED_TO_STAGE, addedHandler);
    }

    // PUBLIC METHODS
    public function getTxt():String {
        /*
            This method must return the 'txt' property
            of the currently selected option
        */
        return _selected_text;
    }

    public function getValue():String {
        /*
            This method must return the 'val' property
            of the currently selected option
        */
        return _selected_value;
    }

    public function populate(options:Array):void {
        /*
            This method must take an array as an argument,
            and save it to its internal _options property.
            The "options" argument will be a JSON array.
            Each item in this array will be EITHER a String,
            OR an object with the properties 'txt' and 'val'
        */
        /* save it as a copy of initial array */
        _options = _options || [];
        _options = options.concat();
//        _options.length = Math.min(_config.itemsLimit, _options.length);
        build();
    }

    public function selectIndex(i:int):void {
        /*
            This method must take an integer as an argument,
            and update its "selected" properties accordingly
        */
        const valid: Boolean = i >= 0 && i > (_items.length-1);
        if (valid) {
            const data: Object = _values[i];
            _selected_index = i;
            _selected_text = _options[i].txt;
            _selected_value = _options[i].val;
            setSelection(data);
        } else {
            setSelection(null);
        }
    }

    public function scale(val: Number): void {
        scaleX = scaleY = val;
        _scroll && _scroll.scale(1/val);
    }
    public function move(_x: int, _y: int): void {
        x = _x;
        y = _y;
    }

    /* protected methods */
    protected function reset(): void {
        _items.map(function(item: Sprite, ind: int, arr: Vector.<Sprite>): void {
            item.parent && item.parent.removeChild(item);
        });
        _items.length = 0;
        _values.length = 0;
        _itemsHash = new Dictionary;
        _state = null;

        _marker && (_marker.visible = false);
        _mask && _mask.reset();
        _inMask && _inMask.reset();

        if (_selection) {
            _selection.dispose();
            _selection = null;
        }
        _menu && _menu.reset();
        if (_scroll) {
            _scroll.dispose();
            _scroll = null;
        }

        setSelection(null);
        stage && stage.removeEventListener(MouseEvent.CLICK, stageHandler);
    }
    protected function build(): void {
        reset();
        const uiFactory: UIFactory = UIFactory.getInstance();
        _options = _options || [];
        if (!_holder || !_options.length) return;

        /* adding menu * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
        var Y: int = _config.firstGap, itemHeight: Number = 0, maxWidth: Number = -Infinity, longestLabel: String = '';
        var backH: int;
        _options.map(function (opt: Object, ind: int, arr: Array): void {
            const data: Object = (typeof opt === 'string') ?
                    {txt: opt, val: ind} :
                    opt;
            const item: ListItem = uiFactory.getListItem(data, false, _config, listHandler);
            _holder.addChild(item);
            _itemsHash[item] = data;
            _items.push(item);
            _values.push(data);

            itemHeight = item.height;
            maxWidth = Math.max(item.width, maxWidth);
            longestLabel = (data.txt.length > longestLabel.length) ? String (data.txt) : longestLabel;

            item.y = Y;
            Y += _config.gap + item.height;

            if (ind < _config.itemsLimit) backH = Y;
        });

        Y -= _config.gap;
        Y += _config.lastGap;
        backH -= _config.gap;
        backH += _config.lastGap;

        /* adding selection * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
        _selection = uiFactory.getListBack(null, true);
        const sback: Sprite = _selection.getBack();
        const sitem: ListItem = uiFactory.getListItem(
                {txt: longestLabel},
                true,
                _config,
                selectionHandler
        );
        _selection.x = _config.topGap;
        sitem.y = _config.firstGap;
        _selection.addItem(sitem);
        _selection.setSize(
            sitem.width + _config.paddingLeft + _config.paddingRight,
            sitem.height + _config.firstGap + _config.lastGap
        );


        /* finally fixing sizes * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
        const back: Sprite = _menu.getBack();
        _menu.setSize(
            sback.width,
            backH
        );

        _items.map(function (item: ListItem, ind: int, arr: Vector.<Sprite>): void {
            item.setSize(back.width, itemHeight);
        });
        sitem.setSize(back.width, itemHeight);

        _top.width = back.width + _config.topGap * 2;

        _marker.width = back.width;
        _marker.height = itemHeight;

        _inMask.init(back.width, back.height);

        _mask.init(_top.width, back.height+10);
        _menu.y = -_menu.height;

        if (_holder.height > _inMask.height) {
            _scroll = uiFactory.getScrollBar(
                    new ScrollBarConfig(
                        20,
                        (itemHeight * _config.itemsLimit + _config.gap * (_config.itemsLimit-1)) * scaleX,
                        3,
                        5,
                        _holder, 20, _inMask, false, false, false)
            );
            _scroll.scale(1/scaleX);
            _scroll.x = back.width - _scroll.width - 5;
            _scroll.y = _config.firstGap;
        }

        arrange();
        setSelection(null);
    }

    protected function setState(val: String): void {
        if (_state === val) return;
        _state = val;

        if (_state === SelectMenuUI.OPENED) {
            open();
        } else if (_state === SelectMenuUI.CLOSED) {
            close();
        }
    }
    protected function setSelection(data: Object): void {
        if (data) {
            _selected_index = _values.indexOf(data);
            _selected_text = String (data.txt);
            _selected_value = String (data.val);
        } else {
            _selected_index = -1;
            _selected_text = '';
            _selected_value = null;
        }
        if (_selection) {
            _selection.setLabel(_selected_text);
            if (_marker && _selected_index > -1) {
                _marker.visible = true;
                _marker.y = _items[_selected_index].y + _config.selectionOffset;
            } else if (_marker) {
                _marker.visible = false;
            }
        } else if (_marker) {
            _marker.visible = false;
        }

    }
    protected function selectionHandler(ev: MouseEvent): void {
        ev.stopPropagation();
        _state === SelectMenuUI.OPENED ?
            setState(SelectMenuUI.CLOSED) :
            setState(SelectMenuUI.OPENED)
    }
    protected function listHandler(ev: MouseEvent): void {
        ev.stopPropagation();
        if (_state !== SelectMenuUI.OPENED) return;
        setSelection(_itemsHash[ev.target]);
        setState(SelectMenuUI.CLOSED);
    }
    protected function stageHandler(ev: MouseEvent): void {
        if (_menu && !_menu.hitTestPoint(stage.mouseX, stage.mouseY, true)) setState(SelectMenuUI.CLOSED);
    }
    protected function open(): void {
        _menu.open(SelectMenuUI.APPEAR_TIME);
        _selection.close(SelectMenuUI.APPEAR_TIME * .25);
        stage.addEventListener(MouseEvent.CLICK, stageHandler);
        _scroll && _scroll.enabled(true);

        if (_selected_index > -1 && _scroll) {
            _scroll.scrollTo(_selected_index / (_options.length-1));
        }
    }
    protected function close(): void {
        _menu.close(SelectMenuUI.DISAPPEAR_TIME);
        _selection.open(SelectMenuUI.DISAPPEAR_TIME * .25);
        stage.removeEventListener(MouseEvent.CLICK, stageHandler);
        _scroll && _scroll.enabled(false);
    }

    protected function addedHandler(ev: Event): void {
        removeEventListener(Event.ADDED_TO_STAGE, addedHandler);
        initialize();
        _options && build();
    }
    protected function initialize(): void {
        const uiFactory: UIFactory = UIFactory.getInstance();

        _masked = new Sprite;

        _top = uiFactory.getMenuHeader();
        _top.mouseEnabled = false;
        _top.blendMode = BlendMode.MULTIPLY;

        _marker = uiFactory.getListMarker(_config.selectColor);
        _menu = uiFactory.getListBack(_marker, true);
        _holder = _menu.getHolder();
        _menu.x = _config.topGap;

        _mask = uiFactory.getListMask();
        _masked.mask = _mask;

        _inMask = uiFactory.getListMask();
        _holder.mask = _inMask;

        arrange();
    }
    protected function arrange(): void {
        _masked.addChild(_menu);
        _selection && _masked.addChild(_selection);

        addChild(_masked);
        _scroll && _menu.addChild(_scroll);
        _menu.addChild(_inMask);
        addChild(_top);
        addChild(_mask);
    }

}

}