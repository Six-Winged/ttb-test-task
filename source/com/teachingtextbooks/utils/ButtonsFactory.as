package com.teachingtextbooks.utils {

import com.teachingtextbooks.constants.ButtonsKeys;

import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.utils.Dictionary;

public class ButtonsFactory {

    protected var buttonsHash: Dictionary;

    protected static var instance: ButtonsFactory;

    public function ButtonsFactory(priv: ButtonsFactoryPriv) {
        buttonsHash = new Dictionary;
        buttonsHash[ButtonsKeys.DEFAULT] = getArrowUp;
        buttonsHash[ButtonsKeys.ARROW_UP] = getArrowUp;
        buttonsHash[ButtonsKeys.ARROW_DOWN] = getArrowDown;
        buttonsHash[ButtonsKeys.FLAT] = getFlat;
    }
    public static function getInstance(): ButtonsFactory {
        if (!ButtonsFactory.instance) {
            ButtonsFactory.instance = new ButtonsFactory(new ButtonsFactoryPriv);
        }
        return ButtonsFactory.instance;
    }
    public function getButton(key: String, width: uint = 0, height: uint = 0): SimpleButton {
        const handler: Function = buttonsHash[key] || buttonsHash[ButtonsKeys.DEFAULT];
        return handler(width, height);
    }

    protected function getArrowUp(width: uint, height: uint): SimpleButton {
        return getArrow(scroll_up);
    }
    protected function getArrowDown(width: uint, height: uint): SimpleButton {
        return getArrow(scroll_down);
    }
    protected function getFlat(width: uint, height: uint): SimpleButton {
        const up: Sprite = new Sprite;
        const hit: Sprite = new Sprite;

        up.graphics.beginFill(0xeeeeee, 0);
        up.graphics.drawRect(0, 0, width, height);
        up.graphics.endFill();

        hit.graphics.beginFill(0xeeeeee, 1);
        hit.graphics.drawRect(0, 0, width, height);
        hit.graphics.endFill();

        return new SimpleButton(up, up, up, hit);
    }

    protected function getArrow(asset: Class): SimpleButton {
        const up: Sprite = new asset;
        const over: Sprite = new asset;
        const down: Sprite = new asset;

        try {
            const overIn: Sprite = over.getChildByName('mc') as Sprite;
            overIn.y--;
        } catch(err: Object) {};

        const hit: Sprite = new Sprite;
        hit.graphics.beginFill(0xeeeeee, 1);
        hit.graphics.drawRect(0, 0, up.width, up.height);
        hit.graphics.endFill();

        return new SimpleButton(up, over, down, hit);
    }

}

}

class ButtonsFactoryPriv {}