package com.teachingtextbooks.utils {

import flash.display.DisplayObject;

public class ScrollBarConfig {

    protected var _width: uint;
    protected var _height: uint;
    protected var _fixedThumb: Boolean;
    protected var _autoPosition: Boolean;
    protected var _tweenScroll: Boolean;
    protected var _tweenTime: Number;
    protected var _holdInterval: Number;
    protected var _gap: int;
    protected var _padding: int;
    protected var _target: DisplayObject;
    protected var _targetGap: Number;
    protected var _mask: DisplayObject;

    public function ScrollBarConfig(
            width: uint,
            height: uint,
            gap: int,
            padding: int,
            target: DisplayObject,
            targetGap: Number,
            mask: DisplayObject,
            fixedThumb: Boolean = false,
            autoPosition: Boolean = true,
            tweenScroll: Boolean = true,
            tweenTime: Number = .15,
            holdInterval: Number = 150
    ) {
        _width = width;
        _height = height;
        _gap = gap;
        _padding = padding;
        _target = target;
        _targetGap = targetGap;
        _mask = mask;
        _fixedThumb = fixedThumb;
        _autoPosition = autoPosition;
        _tweenScroll = tweenScroll;
        _tweenTime = tweenTime;
        _holdInterval = holdInterval;
    }

    public function get width(): uint {
        return _width;
    }
    public function get height(): uint {
        return _height;
    }
    public function get gap(): int {
        return _gap;
    }
    public function get targetGap(): int {
        return _targetGap;
    }
    public function get padding(): int {
        return _padding;
    }
    public function get autoPosition(): Boolean {
        return _autoPosition;
    }
    public function get fixedThumb(): Boolean {
        return _fixedThumb;
    }
    public function get tweenScroll(): Boolean {
        return _tweenScroll;
    }
    public function get tweenTime(): Number {
        return _tweenTime;
    }
    public function get holdInterval(): Number {
        return _holdInterval;
    }
    public function get target(): DisplayObject {
        return _target;
    }
    public function get mask(): DisplayObject {
        return _mask;
    }

    public function dispose(): void {
        _target = null;
        _mask = null;
    }

    public function clone(): ScrollBarConfig {
        return new ScrollBarConfig(
                _width,
                _height,
                _gap,
                _padding,
                _target,
                _targetGap,
                _mask,
                _fixedThumb,
                _autoPosition,
                _tweenScroll,
                _tweenTime,
                _holdInterval
        );
    }

}
}
