package com.teachingtextbooks.utils {
public class SelectMenuUIConfig {

    protected var _itemsLimit: uint;
    protected var _selectColor: uint;
    protected var _firstGap: Number;
    protected var _gap: Number;
    protected var _lastGap: Number;
    protected var _paddingLeft: Number;
    protected var _paddingRight: Number;
    protected var _topGap: Number;
    protected var _arrowGap: Number;
    protected var _selectionOffset: Number;

    public function SelectMenuUIConfig(
            itemsLimit: uint,
            selectColor: uint,
            firstGap: Number,
            gap: Number,
            lastGap: Number,
            paddingLeft: Number,
            paddingRight: Number,
            arrowGap: Number = 10,
            topGap: Number = 40,
            selectionOffset: Number = 6
    ) {
        _itemsLimit = itemsLimit;
        _selectColor = selectColor;
        _firstGap = firstGap;
        _gap = gap;
        _lastGap = lastGap;
        _paddingLeft = paddingLeft;
        _paddingRight = paddingRight;
        _topGap = topGap;
        _arrowGap = arrowGap;
        _selectionOffset = selectionOffset;
    }

    public function get itemsLimit(): uint {
        return _itemsLimit;
    }
    public function get selectColor(): uint {
        return _selectColor;
    }
    public function get firstGap(): Number {
        return _firstGap;
    }
    public function get gap(): Number {
        return _gap;
    }
    public function get lastGap(): Number {
        return _lastGap;
    }
    public function get paddingLeft(): Number {
        return _paddingLeft;
    }
    public function get paddingRight(): Number {
        return _paddingRight;
    }
    public function get topGap(): Number {
        return _topGap;
    }
    public function get arrowGap(): Number {
        return _arrowGap;
    }
    public function get selectionOffset(): Number {
        return _selectionOffset;
    }

    public function clone(): SelectMenuUIConfig {
        return new SelectMenuUIConfig(
                _itemsLimit,
                _selectColor,
                _firstGap,
                _gap,
                _lastGap,
                _paddingLeft,
                _paddingRight,
                _arrowGap,
                _topGap,
                _selectionOffset
        );
    }

}
}
