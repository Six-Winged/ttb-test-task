package com.teachingtextbooks.utils {

import com.teachingtextbooks.ui.ListHolder;
import com.teachingtextbooks.ui.ListItem;
import com.teachingtextbooks.ui.ListMask;
import com.teachingtextbooks.ui.ListScrollBar;
import com.teachingtextbooks.ui.ListScrollBarThumb;

import flash.display.Sprite;
import flash.geom.Rectangle;

public class UIFactory {

    protected static var instance: UIFactory;

    public function UIFactory(priv: UIFactoryPriv) {
    }
    public static function getInstance(): UIFactory {
        if (!UIFactory.instance) {
            UIFactory.instance = new UIFactory(new UIFactoryPriv());
        }
        return UIFactory.instance;
    }

    public function getListBack(marker: Sprite, shadowed: Boolean): ListHolder {
        return new ListHolder(marker, shadowed);
    }
    public function getListMarker(color: uint): Sprite {
        const ret: Sprite = new Sprite;
        ret.graphics.beginFill(color, 1);
        ret.graphics.drawRect(0, 0, 1, 1);
        ret.graphics.endFill();
        ret.mouseEnabled = false;
        ret.visible = false;
        return ret;
    }
    public function getListItem(data: Object, arrowed: Boolean = false, config: SelectMenuUIConfig = null, tapHandler: Function = null): ListItem {
        return new ListItem(data, arrowed, config, tapHandler);
    }
    public function getListMask(): ListMask {
        return new ListMask;
    }
    public function getMenuHeader(): Sprite {
        return new selectmenu_slot;
    }
    public function getScrollTrack(): Sprite {
        const ret: Sprite = new scroll_track;
        ret.scale9Grid = new Rectangle(2, 2, ret.width-4, ret.height-4);
        return ret;
    }
    public function getScrollBar(config: ScrollBarConfig): ListScrollBar {
        return new ListScrollBar(config);
    }
    public function getScrollBarThumb(): ListScrollBarThumb {
        return new ListScrollBarThumb;
    }

}

}

class UIFactoryPriv {}